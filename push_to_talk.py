#/usr/bin/env python3

import evdev
import asyncio
import subprocess

#PUSH_TO_TALK_KEY = keyboard.Key.caps_lock
# Deactivated caps lock keycode
#PUSH_TO_TALK_KEY = keyboard.KeyCode(vk=16777215)
PUSH_TO_TALK_KEY = evdev.ecodes.ecodes['KEY_CAPSLOCK']


def call_cmd(cmd: str):
	print(cmd)
	process = subprocess.Popen(cmd.split(" "), stdout=subprocess.PIPE)
	output, err = process.communicate()
	return output, err


def get_microphones():
	output, _ = call_cmd("pactl list sources short")
	output = output.decode("UTF-8")
	mics = []
	for row in output.split("\n"):
		items = row.split()
		if len(items) > 2 and not items[1].endswith("monitor"):
			mics.append(items[1])
	return mics



class PushToTalk:
	def __init__(self, microphone: str, key = PUSH_TO_TALK_KEY):
		self.microphone = microphone
		self.key = key
		self.delay_thread = None
		self.last_release = 0
		self.devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
		self.devices = list(filter(lambda x: "keyboard" in x.name.lower(), self.devices))
		print(self.devices)
	

	async def event_loop(self, device):
		async for event in device.async_read_loop():
			if event.value == 0:
				self.on_release(event.code)
			if event.value == 1:
				self.on_press(event.code)

	
	def run(self):
		for device in self.devices:
			asyncio.ensure_future(self.event_loop(device))

		loop = asyncio.get_event_loop()
		loop.run_forever()

	def set_mute(self, state):
		cmd = "pactl set-source-mute {} {}".format(self.microphone, state)
		print(cmd)
		subprocess.Popen(cmd.split())

	def mute_mic(self):
		self.set_mute(1)
	
	def unmute_mic(self):
		self.set_mute(0)
	
	def on_press(self, key):
		if key == self.key:
			self.last_release = 0
			self.unmute_mic()
	
	def on_release(self, key):
		if key == self.key:
			self.mute_mic()

if __name__ == "__main__":
	mics = get_microphones()
	print(mics)
	push_to_talk = PushToTalk("@DEFAULT_SOURCE@")
	#push_to_talk = PushToTalk(mics[0])
	push_to_talk.run()
